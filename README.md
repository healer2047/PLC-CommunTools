# CommunTools
> * 此工具针对于Windows工业 **PLC** 硬件通讯协议及基础通讯而编写的通讯调试/测试工具，采用C#8.0语法及.NET Framework 4.6.2框架。
>
> * 此项目暂时先开发winform版本，后期会提供WPF版本。
>
> * 本人会把多年从事工业行业的硬件通讯经验体现在工具中，同时有相关行业经验的开发人员也可以提供思路和建议。
>
> * 2020年前后写的，很早的代码了，欢迎加入及学习
> * 效果图预览：
> 
> ![Image](/ShotCuts/tools.PNG)
> ![Image](/ShotCuts/server.PNG)
> ![Image](/ShotCuts/client.PNG)
